<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('questions')->delete();
        
        \DB::table('questions')->insert(array (
            0 => 
            array (
                'id' => 3,
                'question' => 'qus1sec1',
                'codesnip' => NULL,
                'section' => 1,
                'quizid' => 1,
                'created_at' => '2019-03-21 08:35:26',
                'updated_at' => '2019-03-21 08:35:26',
            ),
            1 => 
            array (
                'id' => 4,
                'question' => 'qus1sec2',
                'codesnip' => NULL,
                'section' => 2,
                'quizid' => 1,
                'created_at' => '2019-03-21 08:35:49',
                'updated_at' => '2019-03-21 08:35:49',
            ),
            2 => 
            array (
                'id' => 5,
                'question' => 'qus',
                'codesnip' => NULL,
                'section' => NULL,
                'quizid' => 1,
                'created_at' => '2019-03-21 08:53:20',
                'updated_at' => '2019-03-21 08:53:20',
            ),
            3 => 
            array (
                'id' => 6,
                'question' => 'qus2sec1',
                'codesnip' => NULL,
                'section' => 1,
                'quizid' => 1,
                'created_at' => '2019-03-21 09:36:39',
                'updated_at' => '2019-03-21 09:36:39',
            ),
            4 => 
            array (
                'id' => 7,
                'question' => 'qus3sec1',
                'codesnip' => NULL,
                'section' => 1,
                'quizid' => 1,
                'created_at' => '2019-03-21 09:37:01',
                'updated_at' => '2019-03-21 09:37:01',
            ),
            5 => 
            array (
                'id' => 8,
                'question' => 'qus4sec`',
                'codesnip' => '<include>*stdio.h',
                'section' => 1,
                'quizid' => 1,
                'created_at' => '2019-03-21 13:03:38',
                'updated_at' => '2019-03-21 13:03:38',
            ),
        ));
        
        
    }
}