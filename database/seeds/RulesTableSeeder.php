<?php

use Illuminate\Database\Seeder;

class RulesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('rules')->delete();
        
        \DB::table('rules')->insert(array (
            0 => 
            array (
                'id' => 1,
                'quizId' => 1,
                'heading' => 'General Gudelines',
                'rules' => 'Rule 1*Rule 2*Rule 3',
                'created_at' => '2019-03-19 13:48:01',
                'updated_at' => '2019-03-19 13:48:01',
            ),
            1 => 
            array (
                'id' => 3,
                'quizId' => 1,
                'heading' => 'Programming Rules',
                'rules' => 'Do not cheat*Fine of rs 500',
                'created_at' => '2019-03-20 06:39:23',
                'updated_at' => '2019-03-20 06:39:23',
            ),
        ));
        
        
    }
}