<?php

use Illuminate\Database\Seeder;

class ResponseCodesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('response_codes')->delete();
        
        \DB::table('response_codes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'answered',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'review later',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}