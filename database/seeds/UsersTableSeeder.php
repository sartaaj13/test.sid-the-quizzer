<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Inder',
                'email' => 'inder@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$AKezG/mvGNkFTqxVYmvJTu5afWQX5H9e4cwGu7EYnDO9vcHduLfLG',
                'remember_token' => NULL,
                'created_at' => '2019-03-19 15:38:53',
                'updated_at' => '2019-03-19 15:38:53',
            ),
        ));
        
        
    }
}