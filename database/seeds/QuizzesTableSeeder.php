<?php

use Illuminate\Database\Seeder;

class QuizzesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('quizzes')->delete();
        
        \DB::table('quizzes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'IT Quiz_Round 1',
                'key' => 'oidRQ_o1',
                'active' => 0,
                'loginkey' => 'sizzling',
                'created_at' => '2019-03-19 13:31:57',
                'updated_at' => '2019-03-19 13:31:57',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'IT Quiz_Round 2',
                'key' => ' _zIdRRI',
                'active' => 0,
                'loginkey' => 'sizzling',
                'created_at' => '2019-03-19 13:33:43',
                'updated_at' => '2019-03-19 13:33:43',
            ),
        ));
        
        
    }
}