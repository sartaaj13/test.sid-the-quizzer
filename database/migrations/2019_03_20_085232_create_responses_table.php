<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('pid')->unsigned();
            $table->integer('qusid')->unsigned();
            $table->integer('optid')->unsigned()->nullable();
            $table->integer('rescode')->unsigned();
            $table->integer('quizid')->unsigned();
            $table->timestamps();

            $table->foreign('pid')
                  ->references('id')->on('participants')
                  ->onUpdate('CASCADE')
                  ->onDelete('CASCADE');

            $table->foreign('qusid')
                  ->references('id')->on('questions')
                  ->onUpdate('CASCADE')
                  ->onDelete('CASCADE');
            $table->foreign('optid')
                  ->references('id')->on('options')
                  ->onUpdate('CASCADE')
                  ->onDelete('CASCADE');
            $table->foreign('rescode')
                  ->references('id')->on('response_codes')
                  ->onUpdate('CASCADE')
                  ->onDelete('CASCADE');
            $table->foreign('quizid')
                  ->references('id')->on('quizzes')
                  ->onUpdate('CASCADE')
                  ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responses');
    }
}
