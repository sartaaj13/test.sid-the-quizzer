<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('pid')->unsigned();
            $table->integer('quizid')->unsigned();
            $table->integer('correct');
            $table->integer('incorrect');
            $table->integer('score')->default(0);
            $table->timestamps();

            $table->foreign('pid')
                  ->references('id')->on('participants')
                  ->onUpdate('CASCADE')
                  ->onDelete('CASCADE');
            $table->foreign('quizid')
                  ->references('id')->on('quizzes')
                  ->onUpdate('CASCADE')
                  ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scores');
    }
}
