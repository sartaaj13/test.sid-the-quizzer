<?php

use App\Quiz;
use App\Option;
use App\Question;
use App\Rule;
use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.welcome');
})->name('SIDhome');
Auth::routes();
Route::get('admin',function(){
	return view('backend.welcome');
});
Route::group(['middleware' => ['auth']], function() {
	
	Route::get('add-quiz-page', function(){
	return view('backend.create_quiz');
	})->name('add_quiz_page');
	Route::post('/add-quiz', 'QuizController@addQuiz')->name('add_quiz');
	Route::get('view-quiz-page', function(){
		$quiz = Quiz::get();
		return view('backend.view_quiz',compact('quiz'));
	})->name('view_quiz_page');

	Route::get('add-rules-page',['as' => 'add_rule_page', function(){
		$quiz = Quiz::get();
		return view('backend.add_rule_page', compact('quiz'));
	} ]);
	Route::post('add-rules', 'QuizController@addRules')->name('add_rule');
	Route::get('/home', ['as'=>'home','uses'=>'HomeController@index']);
	Route::get('make-inactive/{id}', function($id){
		Quiz::where('id','=',$id)->update(['active'=>0]);
		return Redirect::route('view_quiz_page');
	})->name('make-inactive');
	Route::get('make-active/{id}', function($id){
		Quiz::where('id','=',$id)->update(['active'=>1]);
		return Redirect::route('view_quiz_page');
	})->name('make-active');
	Route::get('delete-quiz/{id}', function($id){
		Quiz::where('id','=',$id)->delete();
		return Redirect::route('view_quiz_page');
	})->name('delete-quiz');
	Route::get('add-questions-page', function(){
		$quiz = Quiz::where('active','=',1)->get();
		return view('backend.add_question', compact('quiz'));
	})->name('add_questions_page');
	Route::get('add-options', ['as' => 'add_options','uses' => 'QuestionController@addOptions']);
	Route::get('view-questions-page', ['as' => 'view_questions_page','uses' => 'QuestionController@viewQuestion']);
	Route::get('deletequestion/{id}', function($id){
		Option::where('qusid','=',$id)->delete();
		Question::where('id','=',$id)->delete();
		return Redirect::route('view_question');
	})->name('deletequestion');
	Route::get('delete-all', function(){
		$quiz = Quiz::where('key','=',session('quizkey'))->get();
		$ques = Question::where('quizid','=',$quiz[0]->id);
		foreach ($ques as $q) {
			Option::where('qusid','=',$q->id)->delete();
		}
		foreach ($ques as $q) {
			Question::where('id','=',$q->id)->delete();
		}
		return Redirect::route('view_question');
	})->name('delete_all');
	Route::get('view-question', ['as' => 'view_question', 'uses' => 'QuizController@viewquestion']);
	Route::post('view-results',['as' => 'view_results','uses' => 'MarkingController@getResult']);
	Route::post('upload-excel',['as' => 'upload_excel','uses' => 'QuestionController@uploadExcel']);

});
Route::get('get-sections','QuestionController@getSection');
Route::post('get-quiz',['as' => 'get_quiz','uses' => 'QuizController@getQuiz']);
Route::get('search-quiz', function(){
	return view('frontend.search_quiz');
})->name('search_quiz');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('view-rule',['as' => 'view_rules', 'uses' => 'QuizController@viewRules']);
Route::post('add-user',['as' => 'add_user', 'uses' => 'QuizController@addUser']);
Route::get('start-quiz',function(){
	return view('frontend.start_quiz');
})->name('start_quiz');
Route::get('new-quiz',['as' => 'new_quiz','uses' => 'QuizController@newQUiz']);
Route::get('get-questions/{section}',['as' => 'get_questions','uses' => 'QuestionController@getQuestions']);
Route::get('forPage','QuestionController@forPage')->name('forPage');
Route::get('addAnswer','MarkingController@addAnswer');
Route::get('removeAnswer','MarkingController@removeAnswer');
Route::get('submit-quiz-home',['as' => 'submit_quiz_home', 'uses' => 'MarkingController@submitQuiz']);
// Route::get('submit-quiz-home', 'MarkingController@submitQuiz2')->name(submit-quiz_home);
Route::get('quizEndsHere',function(){
	return view('Quiz.quizEnds');
})->name('quiz_ends');
Route::get('result','MarkingController@calculateResult')->name('res');
Route::post('view-results','MarkingController@viewResult')->name('view_results');

Route::get('quiz-master', function(){
	return view('master.welcome');
})->name('quiz_master');
Route::get('q-m-login',function(){
	return view('master.login');
})->name('q_m_login');
Route::post('master-login',['as' => 'master_login','uses' => 'MasterController@getLogin']);
Route::group(['middleware' => ['quizmaster']],function(){
	Route::get('master-home', function(){
		return view('master.home');
	})->name('master_home');
	Route::get('q-m-logout', function(){
		if(session()->has('quizmaster'))
		{
			session()->flush();
		}
		return view('master.welcome');
	})->name('q_m_logout');
	Route::get('q-m-view-quiz',['as' => 'master_home', 'uses' => 'MasterController@showQuizPage']);
	Route::get('q-m-inactive', function(){
		Quiz::where('id','=',session('quizmaster'))->update(['active'=>0]);
		return Redirect::route('q_m_view_quiz');
	})->name('q_m_inactives');
	Route::get('q-m-active', function(){
		Quiz::where('id','=',session('quizmaster'))->update(['active'=>1]);
		return Redirect::route('q_m_view_quiz');
	})->name('q_m_actives');
	Route::get('q-m-addquestion-page', function(){
		return view('master.addquestionpage');
	})->name('q_m_addquestion_page');
	Route::get('q-m-addquestion',['as' => 'q_m_addquestion', 'uses' => 'MasterController@addOptions']);
	Route::get('q-m-viewquestion', ['as' => 'q_m_viewquestion', 'uses' => 'MasterController@viewquestion']);
	Route::get('q-m-delete/{id}', function($id){
		Option::where('qusid','=',$id)->delete();
		Question::where('id','=',$id)->delete();
		return Redirect::route('q_m_viewquestion');
	})->name('q_m_delete');
	Route::get('q-m-deleteall', function(){
		$ques = Question::where('quizid','=',session('quizmaster'))->get();
		foreach ($ques as $q) {
			Option::where('qusid','=',$q->id)->delete();
		}
		foreach ($ques as $q) {
			Question::where('id','=',$q->id)->delete();
		}
		return Redirect::route('q_m_viewquestion');
	})->name('q_m_deleteall');
	Route::get('q-m-addrules-page', function(){
		return view('master.addrules');
	})->name('q_m_addrules_page');
	Route::post('q-m-addrules',['as' => 'q_m_addrules','uses' => 'MasterController@addrules']);
	Route::get('q-m-viewrules',['as' => 'q_m_viewrules', 'uses' => 'MasterController@viewrules']);
	Route::get('q-m-delete-rule/{head}', function($heading) {
		Rule::where('heading','=',$heading)->delete();
		return Redirect::route('q_m_viewrules');
	})->name('q_m_delete_rule');
	Route::get('q-m-delete-rule-all', function(){
		Rule::where('quizId','=',session('quizmaster'))->delete();
		return Redirect::route('q_m_addrules_page');
	})->name('q_m_delete_rule_all');
	Route::get('q-m-result',['as' => 'q_m_result', 'uses' => 'MasterController@viewresult']);
	Route::get('q-m-participants',['as' => 'q_m_participants', 'uses' => 'MasterController@getparticipants']);
});