<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    public function scopeFilterByQuizKey($query, $qkey)
    {
    	return $query->where('key','=',$qkey);
    }
    public function scopeFilterByLoginKey($query, $lkey)
    {
    	return $query->where('loginkey','=',$lkey);
    }
}
