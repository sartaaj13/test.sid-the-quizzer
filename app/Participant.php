<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    public function scopeFilterByMail($query, $mail)
    {
    	return $query->where('email','=',$mail);
    }
    public function scopeFilterByTid($query, $tid)
    {
    	return $query->where('tid','=',$tid);
    }
    public function scopeFilterByQid($query, $qid)
    {
    	return $query->where('quiz_id','=',$qid);
    }
    public function scopeFilterByName($query, $name)
    {
        return $query->where('name','=',$name);
    }
}
