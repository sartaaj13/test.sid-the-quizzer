<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    public function scopeFilterByPid($query, $pid)
    {
    	return $query->where('pid','=',$pid);
    }
    public function scopeFilterByQusID($query, $qid)
    {
    	return $query->where('qusid','=',$qid);
    }
    public function scopeFilterByOption($query, $optid)
    {
    	return $query->where('optid','=',$optid);
    }
    public function scopeFilterByQuizId($query, $quizid)
    {
        return $query->where('quizid','=',$quizid);
    }
}
