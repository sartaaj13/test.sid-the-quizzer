<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class QuizMaster
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('quizmaster')==null)
        {
            return redirect('quiz-master');
        }
        return $next($request);
    }
}
