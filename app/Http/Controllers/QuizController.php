<?php

namespace App\Http\Controllers;

use App\Quiz;
use App\Rule;
use App\Participant;
use App\Question;
use Session;
use Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB; 

class QuizController extends Controller
{
    public function addQuiz()
    {
    	$name = Input::get('quiz');
    	$key = $this->generateKey($name);
    	$quiz = new Quiz;
    	$quiz->name = $name;
    	$quiz->key = $key;
        $quiz->loginkey = Input::get('loginkey');
    	$quiz->active = 0;
    	$quiz->save();
        Alert::success($key , 'Quiz Created With Key:');
        return Redirect::route('add_quiz_page');
    }
    public function generateKey($input)
    {
    	$input_length = strlen($input);
    	$random_string = '';
    	for($i = 0; $i < 8; $i++) {
        	$random_character = $input[mt_rand(0, $input_length - 1)];
        	$random_string .= $random_character;
    	}
 
   		 return $random_string;
    }
    public function addRules()
    {
    	$heading = Input::get('heading');
    	$quiz = Input::get('quiz');
    	$rules = Input::get('rules');
        $r = Rule::FilterByQuizId($quiz)->FilterByHeading($heading)->get();
        if(count($r)==1)
        {
            Alert::error('Rules Already Exists. Try Other Heading','Oops!!!!');
            return Redirect::route('add_rule_page');
        }
    	$rule = new Rule;
    	$rule->quizId = $quiz;
    	$rule->heading = $heading;
    	$rule->rules = $rules;
    	$rule->save();
        Alert::success('Rules Added for Quiz','Success!!');
        return Redirect::route('add_rule_page');
    }
    public function viewRules()
    {
    	$qid = session('quizId');
    	$rule = Rule::where('quizId','=',$qid)->get();
        if(count($rule)==0){

        }
    	foreach ($rule as $rules) {
    		$r[$rules->heading] = explode('*', $rules->rules);
    	}
    	return view('frontend.view_rules',compact('r'));
    }
    public function getQuiz()
    {
        $key = Input::get('key');
        $quiz = Quiz::where('key','=',$key)->get();
        if(count($quiz)==0)
        {
            Alert::error('Quiz Not Found. Wrong Key','Oops!!!');
            return Redirect::route('search_quiz');
        }
        else
        {
            session(['quizId' => $quiz[0]->id]);
            return Redirect::route('view_rules');
        }
    }
    public function addUser()
    {
        $name = Input::get('name');
        $mail = Input::get('email');
        $tid = Input::get('tid');
        $quizid = session('quizId');

        $user = Participant::FilterByMail($mail)->FilterByTid($tid)->FilterByQid($quizid)->FilterByName($name)->get();
        if(count($user)!=0)
        {
            if($user[0]->issubmitted==0)
            {
                //resume
                Alert::error('Already Taken Quiz','Oops!!');
                return Redirect::route('view_rules');
            }
            else
            {
                Alert::error('Already Taken Quiz','Oops!!');
                return Redirect::route('view_rules');
            }
        }

        $participant = new Participant;
        $participant->name = $name;
        $participant->email = $mail;
        $participant->tid = $tid;
        $participant->quiz_id = $quizid;
        $participant->save();
        session(['pid' => $participant->id]);
        return Redirect::route('start_quiz');
    }
    public function newQUiz()
    {
        $quizid = session('quizId');
        $section = DB::table('questions')
                 ->where('quizid','=',$quizid)
                 ->select('section', DB::raw('count(*) as total'))
                 ->groupBy('section')
                 ->get();
        return view('Quiz.quiz', compact('section'));
    }
    
    public function viewquestion(Request $request)
    {
        if(session()->has('quizkey'))
        {
            $qid = session('quizkey');
        }
        else
        {
            $qid = $request->quizkey;
            session(['quizkey' => $qid]);
        }
        $sections = DB::table('questions')
                 ->where('quizid','=',$qid)
                 ->select('section', DB::raw('count(*) as total'))
                 ->groupBy('section')
                 ->get();
        $questions = Question::where('quizid','=',session('quizmaster'))->get();
        $option = array();

        foreach($questions as $qus)
        {
            $option = Option::where('qusid','=',$qus->id)->get();
            $qus->option = $option; 
            $code = array();
            if($qus->codesnip!=null)
            {
                $code = explode('*', $qus->codesnip);
            }
            $qus->code = $code;
            
        }
        return view('backend.viewquestion',compact('questions'));
    }
}
