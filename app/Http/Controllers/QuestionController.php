<?php

namespace App\Http\Controllers;
use App\Quiz;
use App\Rule;
use App\Participant;
use Session;
use Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\DB;
use App\Question;
use App\Option;
use App\Response;
use Excel;

class QuestionController extends Controller
{
    public function addQuestion($request)
    {
    	$qus = new Question;
    	$qus->question = $request->qus;
    	$qus->codesnip = $request->code;
    	$qus->quizid = $request->quiz;
    	$qus->section = $request->section;

    	if($qus->save())
    	{
    		return $qus->id;
    	}
    }
    public function addOptions(Request $request)
    {
    	
    	$qusid = $this->addQuestion($request);
    	
    	$option = array('1' => 0,'2'=>0,'3'=>0,'4'=>0 );
    	$option[$request->correct] = 1;
    	$opt1 = new Option;
    	$opt2 = new Option;
    	$opt3 = new Option;
    	$opt4 = new Option;

    	$opt1->qusid = $qusid;
    	$opt1->option = $request->opt1text;
    	$opt1->is_correct = $option[1];

    	$opt2->qusid = $qusid;
    	$opt2->option = $request->opt2text;
    	$opt2->is_correct = $option[2];

    	$opt3->qusid = $qusid;
    	$opt3->option = $request->opt3text;
    	$opt3->is_correct = $option[3];

    	$opt4->qusid = $qusid;
    	$opt4->option = $request->opt4text;
    	$opt4->is_correct = $option[4];

    	if($opt1->save() and $opt2->save() and $opt3->save() and $opt4->save())
    	{
    		Alert::success('Question Inserted','Success!!');
    		return Redirect::route('add_questions_page');
    	}
    	else
    	{
    		Alert::error('Something Went Wrong','Oops!!!!');
    		return Redirect::route('add_questions_page');
    	}

    }
    public function viewQuestion()
    {
        
    }
    public function getSection(Request $request)
    {
        if(session('quizId')==null)
        {
            return Redirect::route('search_quiz');
        }
    	$section = DB::table('questions')
    			 ->where('quizid','=',$request->quizid)
                 ->select('section', DB::raw('count(*) as total'))
                 ->groupBy('section')
                 ->get();
        return $section;
    }
    public function getQuestions($section)
    {
        if(session('quizId')==null)
        {
            return Redirect::route('search_quiz');
        }
    	$qid = session('quizId');
    	// return $section;
    	// return session('pid');
    	$sections = DB::table('questions')
    			 ->where('quizid','=',$qid)
                 ->select('section', DB::raw('count(*) as total'))
                 ->groupBy('section')
                 ->get();
    	$questions = Question::FilterByQuizId($qid)->FilterBySection($section)->inRandomOrder()->get();
    	$option = array();

    	foreach($questions as $qus)
    	{
    		$option = Option::where('qusid','=',$qus->id)->inRandomOrder()->get();
			$qus->option = $option;
			$response = Response::FilterByPid(session('pid'))->FilterByQusId($qus->id)->get();
			
			foreach ($response as $res) {
				$qus->chosen = $res->optid;
				$qus->rescode = $res->rescode;
			}
			// $qus->response = $response;
			// $qus->old = $response[0]->optid;
			// $qus->rescode = $response->rescode;	
    		$code = array();
	    	if($qus->codesnip!=null)
	    	{
	    		$code = explode('*', $qus->codesnip);
	    	}
	    	$qus->code = $code;
    		
    	}
    	// return $response;
    	return view('Quiz.quiz_questions',compact('questions','sections'));
    	// return $questions;
    	// $this->forPage($questions);
  //   	return redirect()->action(
  //   	'QuestionController@forPage', ['questions' => $questions]
		// );

		// $this->forPage($questions);
    }
    public function forPage($questions)
    {
    	// dd($questions);
    	// $questions = $questions->paginate(1);
    	// return view('Quiz.quiz_questions',compact('questions'));
    	

    }
    public function uploadExcel(Request $req)
    {
            // dd($request);
            $file = $req->file('excelfile');
            $path = $file->path();
            $data = Excel::load($path)->get();

            if($data->count()){
            foreach ($data as $key => $request) {


                $qus = new Question;
                $qus->question = $request->qus;
                $qus->codesnip = $request->code;
                $qus->quizid = $req->quiz;
                $qus->section =$request->section;
                $qus->save();
                $qusid = $qus->id;

                $option = array('1' => 0,'2'=>0,'3'=>0,'4'=>0 );
                $option[$request->correct] = 1;
                $opt1 = new Option;
                $opt2 = new Option;
                $opt3 = new Option;
                $opt4 = new Option;
                $opt1->qusid = $qusid;
                $opt1->option = $request->opt1text;
                $opt1->is_correct = $option[1];

                $opt2->qusid = $qusid;
                $opt2->option = $request->opt2text;
                $opt2->is_correct = $option[2];

                $opt3->qusid = $qusid;
                $opt3->option = $request->opt3text;
                $opt3->is_correct = $option[3];

                $opt4->qusid = $qusid;
                $opt4->option = $request->opt4text;
                $opt4->is_correct = $option[4];

                $opt1->save();
                $opt2->save();
                $opt3->save();
                $opt4->save();
               
            }
                Alert::success('Question Inserted','Success!!');
                return Redirect::route('add_questions_page');
        }
    }
}

