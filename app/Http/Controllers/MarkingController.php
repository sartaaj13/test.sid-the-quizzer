<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input; 
use App\Response;
use App\Question;
use App\Option;
use App\Participant;
use Alert;
use App\Scores;
use App\Quiz;

class MarkingController extends Controller
{
    public function addAnswer(Request $request)
    {
    	$qusid = Option::where('id','=',$request->optid)->get('qusid');
    	$responses = Response::FilterByPid(session('pid'))->FilterByQusId($qusid[0]->qusid)->get();
    	if(count($responses)>0)
    	{
    		$responses = Response::FilterByPid(session('pid'))->FilterByQusId($qusid[0]->qusid)->delete();
	    	$res = new Response;
	    	$res->pid = session('pid');
	    	$res->qusid = $qusid[0]->qusid;
	    	$res->optid = $request->optid;
	    	$res->rescode = 1;
	    	$res->quizid = session('quizId');
	    	$res->save();
	    	return response()->json($res);
    	}
    	else
    	{
    		$res = new Response;
	    	$res->pid = session('pid');
	    	$res->qusid = $qusid[0]->qusid;
	    	$res->optid = $request->optid;
	    	$res->rescode = 1;
	    	$res->quizid = session('quizId');
	    	$res->save();
	    	return response()->json($res);
    	}
    }
    public function removeAnswer(Request $request)
    {
    	//$qusid = Option::where('id','=',$request->optid)->get('qusid');
        $opt = Response::FilterByPid(session('pid'))->FilterByQusId($request->qusid)->get();
    	$res = Response::FilterByPid(session('pid'))->FilterByQusId($request->qusid)->delete();
        //dd($opt);
    	return response()->json($opt);
    }
    public function submitQuiz()
    {
        if(session('quizId')==null)
        {
            return Redirect::route('search_quiz');
        }
    	$participant = Participant::where('id','=', session('pid'))->update(['issubmitted' => 1]);
    	$pid = session('pid');
    	$quizid = session('quizId');
    	$answered = Response::FilterByPid($pid)->FilterByQuizId($quizid)->where('rescode','=',1)->get();
    	$correct = array();
    	foreach ($answered as $ans) {
    		$correct[] = Option::FilterByOptId($ans->optid)->FilterByCorrect()->count();
    	}
    	$j=0;
    	for($i=0;$i<count($correct);$i++)
    	{
    		if($correct[$i]==1)
    		{
    			$j++;
    		}
    	}
    	$total = count($answered);
       	$wrongAns = $total - $j;
    	$positive = $j*4;
    	$negative = $wrongAns*2.5;
    	$overall = $positive-$negative;

    	$score = new Scores;
    	$score->pid = session('pid');
    	$score->quizid = session('quizId');
    	$score->correct = $j;
    	$score->incorrect = $wrongAns;
    	$score->score = $overall;
    	if ($score->save()) {
    		session()->flush();
    		Alert::success('Quiz Submitted','Success!!');
    		return view('Quiz.submit_quiz_home');
    	}
    	else
    	{
    		Alert::error('Something went wrong!','Oops!!');
    		return view('Quiz.submit_quiz_home');
    	}
    }
    public function calculateResult()
    {
    	$pid = session('pid');
    	$quizid = session('quizId');
    	$answered = Response::FilterByPid($pid)->FilterByQuizId($quizid)->where('rescode','=',1)->get();
    	$correct = array();
    	foreach ($answered as $ans) {
    		$correct[] = Option::FilterByOptId($ans->optid)->FilterByCorrect()->count();
    	}
    	$j=0;
    	for($i=0;$i<count($correct);$i++)
    	{
    		if($correct[$i]==1)
    		{
    			$j++;
    		}
    	}
    	$total = count($answered);
       	$wrongAns = $total - $j;
    	$positive = $j*4;
    	$negative = $wrongAns*2.5;
    	$overall = $positive-$negative;

    	$score = new Scores;
    	$score->pid = session('pid');
    	$score->quizid = session('quizId');
    	$score->correct = $j;
    	$score->incorrect = $wrongAns;
    	$score->score = $overall;
    	if ($score->save) {
    		return $score;
    	}
    	else
    	{
    		return 0;
    	}
    }
    public function viewResult()
    {
        $key = Input::get('quizkey');
    	$qid = Quiz::where('key','=',$key)->pluck('id');

        $result = Scores::where('quizid','=',$qid)->get();
        foreach ($result as $res) {
            $par = Participant::where('id','=',$res->pid)->select('tid','name')->first();
            // return $par;
            $res->tid = $par->tid;
            $res->name = $par->name;
        }
        $quiz = Quiz::where('id','=',$qid)->pluck('name');
        // dd($result);
        return view('backend.view_results',compact('result','quiz'));
    }
}
