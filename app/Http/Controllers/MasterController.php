<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use App\Rule;
use App\Participant;
use App\Question;
use App\Option;	
use App\Scores;
use Session;
use Alert;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB; 

class MasterController extends Controller
{
    public function getLogin()
    {
    	$qkey = Input::get('quizkey');
    	$lkey = Input::get('loginkey');
    	$response = Quiz::FilterByQuizKey($qkey)->FilterByLoginKey($lkey)->get();
    	if(count($response)==1)
    	{
    		session(['quizmaster' => $response[0]->id]);
    		Alert::success('Logged In','Success');
    		return Redirect::route('master_home'); 
    	}
    	else
    	{

    	}
    }
    public function showQuizPage()
    {
    	$response = Quiz::where('id','=',session('quizmaster'))->get();
    	$question = Question::where('quizid','=',$response[0]->id)->get();
    	$quscout = count($question);
    	return view('master.viewquiz',compact('response','quscout'));
    }
    public function addQuestion($request)
    {
    	$qus = new Question;
    	$qus->question = $request->qus;
    	$qus->codesnip = $request->code;
    	$qus->quizid = session('quizmaster');
    	$qus->section = $request->section;

    	if($qus->save())
    	{
    		return $qus->id;
    	}
    }
    public function addOptions(Request $request)
    {
    	
    	$qusid = $this->addQuestion($request);
    	
    	$option = array('1' => 0,'2'=>0,'3'=>0,'4'=>0 );
    	$option[$request->correct] = 1;
    	$opt1 = new Option;
    	$opt2 = new Option;
    	$opt3 = new Option;
    	$opt4 = new Option;

    	$opt1->qusid = $qusid;
    	$opt1->option = $request->opt1text;
    	$opt1->is_correct = $option[1];

    	$opt2->qusid = $qusid;
    	$opt2->option = $request->opt2text;
    	$opt2->is_correct = $option[2];

    	$opt3->qusid = $qusid;
    	$opt3->option = $request->opt3text;
    	$opt3->is_correct = $option[3];

    	$opt4->qusid = $qusid;
    	$opt4->option = $request->opt4text;
    	$opt4->is_correct = $option[4];

    	if($opt1->save() and $opt2->save() and $opt3->save() and $opt4->save())
    	{
    		Alert::success('Question Inserted','Success!!');
    		return Redirect::route('q_m_addquestion_page');
    	}
    	else
    	{
    		Alert::error('Something Went Wrong','Oops!!!!');
    		return Redirect::route('q_m_addquestion_page');
    	}

    }
    public function uploadExcel(Request $req)
    {
            // dd($request);
            $file = $req->file('excelfile');
            $path = $file->path();
            $data = Excel::load($path)->get();

            if($data->count()){
            foreach ($data as $key => $request) {


                $qus = new Question;
                $qus->question = $request->qus;
                $qus->codesnip = $request->code;
                $qus->quizid = session('quizmaster');
                $qus->section =$request->section;
                $qus->save();
                $qusid = $qus->id;

                $option = array('1' => 0,'2'=>0,'3'=>0,'4'=>0 );
                $option[$request->correct] = 1;
                $opt1 = new Option;
                $opt2 = new Option;
                $opt3 = new Option;
                $opt4 = new Option;
                $opt1->qusid = $qusid;
                $opt1->option = $request->opt1text;
                $opt1->is_correct = $option[1];

                $opt2->qusid = $qusid;
                $opt2->option = $request->opt2text;
                $opt2->is_correct = $option[2];

                $opt3->qusid = $qusid;
                $opt3->option = $request->opt3text;
                $opt3->is_correct = $option[3];

                $opt4->qusid = $qusid;
                $opt4->option = $request->opt4text;
                $opt4->is_correct = $option[4];

                $opt1->save();
                $opt2->save();
                $opt3->save();
                $opt4->save();
               
            }
                Alert::success('Question Inserted','Success!!');
                return Redirect::route('q_m_addquestion_page');
        }
    }
    public function viewquestion()
    {
    	$qid = session('quizmaster');
    	$sections = DB::table('questions')
    			 ->where('quizid','=',$qid)
                 ->select('section', DB::raw('count(*) as total'))
                 ->groupBy('section')
                 ->get();
    	$questions = Question::where('quizid','=',session('quizmaster'))->get();
    	$option = array();

    	foreach($questions as $qus)
    	{
    		$option = Option::where('qusid','=',$qus->id)->get();
			$qus->option = $option;	
    		$code = array();
	    	if($qus->codesnip!=null)
	    	{
	    		$code = explode('*', $qus->codesnip);
	    	}
	    	$qus->code = $code;
    		
    	}
    	return view('master.viewquestions',compact('questions'));
    }
    public function addrules()
    {
    	$heading = Input::get('heading');
    	$quiz = session('quizmaster');
    	$rules = Input::get('rules');
        $r = Rule::FilterByQuizId($quiz)->FilterByHeading($heading)->get();
        if(count($r)==1)
        {
            Alert::error('Rules Already Exists. Try Other Heading','Oops!!!!');
            return Redirect::route('q_m_addrules_page');
        }
    	$rule = new Rule;
    	$rule->quizId = $quiz;
    	$rule->heading = $heading;
    	$rule->rules = $rules;
    	$rule->save();
        Alert::success('Rules Added for Quiz','Success!!');
        return Redirect::route('q_m_addrules_page');
    }
    public function viewrules()
    {
    	$qid = session('quizmaster');
    	$rule = Rule::where('quizId','=',$qid)->get();
        if(count($rule)==0){
        	Alert::error('No Rules Found','Oops!!');
        	return Redirect::route('q_m_addrules_page');
        }
    	foreach ($rule as $rules) {
    		$r[$rules->heading] = explode('*', $rules->rules);
    	}
    	return view('master.viewrules',compact('r'));
    }
    public function viewresult()
    {
    	$qid = session('quizmaster');

        $result = Scores::where('quizid','=',$qid)->get();
        foreach ($result as $res) {
            $par = Participant::where('id','=',$res->pid)->select('tid','name')->first();
            // return $par;
            $res->tid = $par->tid;
            $res->name = $par->name;
        }
        $quiz = Quiz::where('id','=',$qid)->pluck('name');
        // dd($result);
        return view('master.viewresults',compact('result','quiz'));
    }
    public function getparticipants()
    {
    	$par = Participant::where('quiz_id','=',session('quizmaster'))->get();
    	return view('master.viewparticipants',compact('par'));
    }
}
