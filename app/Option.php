<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    public function scopeFilterByOptId($query, $optid)
    {
    	return $query->where('id','=',$optid);
    }
    public function scopeFilterByCorrect($query)
    {
    	return $query->where('is_correct','=',1);
    }
}
