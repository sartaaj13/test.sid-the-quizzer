<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    public function scopeFilterByQuizId($query, $qid)
    {
    	return $query->where('quizId','=',$qid);
    }
    public function scopeFilterByHeading($query, $heading)
    {
    	return $query->where('heading','=',$heading);
    }
}
