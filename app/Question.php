<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function scopeFilterByQuizId($query, $qid)
    {
    	return $query->where('quizid','=',$qid);
    }
    public function scopeFilterBySection($query, $section)
    {
    	return $query->where('section','=',$section);
    }
}
