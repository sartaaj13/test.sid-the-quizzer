@extends('Quiz.layouts.app')
@section('custom-styles')
<style>
@font-face {
            font-family: 'myFont';
            src: url('{{asset('css/fonts/Billabong.ttf')}}');
            }
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection()
@section('content')
@include('sweet::alert')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<h1 class="text-center">Quiz Submitted!!</h1>	
					<br><br><br><br><br>
					<h4 style="font-family: myFont" class="text-center">App Crafted By:</h4>		
					<h1 style="font-family: myFont" class="text-center">Inderjeet Singh</h1>
					<br><br>
					<h6>Wait for 5 seconds</h6>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footer-scripts')
<script>
	window.setTimeout(function(){location.replace("quizEndsHere")}, 5000)
</script>
@endsection()
