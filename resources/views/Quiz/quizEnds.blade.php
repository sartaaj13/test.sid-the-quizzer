<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 100px;
            }
    
            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 20px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
            .links > span {
                color: #636b6f;
                padding: 0 25px;
                font-size: 25px;
                font-weight: 500;
                letter-spacing: .1rem;
                text-decoration: none;
            }
            .m-b-md {
                margin-bottom: 30px;
            }
            @font-face {
            font-family: 'myFont';
            src: url('{{asset('css/fonts/Billabong.ttf')}}');
            }
        </style>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    </head>
    <body>
        @include('sweet::alert')
        <div class="flex-center position-ref full-height">
            <div class="top-right links">
                   <a href="{{ route('SIDhome') }}">Home</a>
                </div>
            <div class="content">
                <div class="links">
                    <span style="font-family: myFont">App Crafted By:</span>
                </div>
                <div class="title m-b-md" style="font-family: myFont">
                    Inderjeet Singh
                </div>

                <div class="links">
                    <a href=""><i class="fas fa-code-branch"></i> Github</a>
                    <a href=""><i class="fab fa-bitbucket"></i> BitBucket</a>
                </div>
            </div>
        </div>
    </body>
</html>
