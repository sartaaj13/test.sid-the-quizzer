@extends('Quiz.layouts.app')
@section('custom-styles')
<style>
	.m-b-15{
		margin-bottom: 15px;
	}
	.answered{
		background: #008000;
		color: #ffffff;
	}
	.review{
		background: #ff8c00;
	}
	.unanswered{
		background: #ff4500;
		color: #ffffff;
	}
	.pull-right{
		float: right;
	}
	.anchor{
		color: #ffffff;
	}
	a:hover{
		color: #f3f3f3f;
	}
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection()
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<button class="btn btn-success" onclick="submitQuiz()">
						Submit
					</button>
					@if(count($sections)>1)
							@foreach($sections as $sec)
								<a href="{{ route('get_questions',['section'=>$sec->section]) }}"><button class="btn btn-primary">Section {{$sec->section}}</button></a>
							@endforeach
					@endif
				</div>
				<div class="card-body">
				@foreach($questions as $ques)
					<div class="row justify-content-center">
						<div class="col-sm-12">
							<div class="card m-b-15">
								<div class="card-header">
									<h3>Qus. {{ $ques->question }}?</h3>
								</div>
								<div class="card-body">
									@if($ques->codesnip!=null)
										<div class="card m-b-15">
											<div class="card-header">Code for the question</div>
											<div class="card-body">
												@foreach($ques->code as $code)
													<kbd>{{ $code }}</kbd><br>
												@endforeach
											</div>
										</div>
									@endif
									<div class="card">
										<div class="card-body">
											<?php $i=1;?>
											@foreach($ques->option as $options)
												@if($ques->chosen!=null)
													@if($options->id == $ques->chosen && $ques->rescode == 1)
														<h3><?=$i?>. {{ $options->option }} <input type="radio" value="{{ $options->id }}" name="{{ $ques->id }}" id="{{ $options->id }}" onclick="addAnswer(this.value)" checked="" class="form-contr-sm">
														</h3>
													@else
														<h3><?=$i?>. {{ $options->option }} <input type="radio" id="{{ $options->id }}" name="{{ $ques->id }}" value="{{ $options->id }}" onclick="addAnswer(this.value)" class="form-contr-sm"></h3>
													@endif
												@else
													<h3><?=$i?>. {{ $options->option }} <input type="radio" id="{{ $options->id }}" name="{{ $ques->id }}" value="{{ $options->id }}" onclick="addAnswer(this.value)" class="form-contr-sm"></h3>										
												@endif
												<?php $i++?>
											@endforeach
										</div>
									</div>
								</div>
								@if($ques->chosen!=null)
								@if($ques->rescode == 1)
									<div id="{{ $ques->id }}">
										<div class="card-footer answered">
											<!-- <h6><a href="" class="anchor">Remove Answer</a></h6> -->
											<button class="btn btn-sm" value="{{ $ques->id }}" onclick="removeAnswer(this.value)">Remove Answer</button>
										</div>
									</div>
								@endif
								@if($ques->rescode == 2)
								<div id="{{ $ques->id }}">
									<div class="card-footer review">
										<h6>Saved to Review Later</h6>
									</div>
								</div>
								@endif
								@else
								<div id="{{ $ques->id }}">
									<div class="card-footer unanswered">
										<h6>Not Answered</h6>
									</div>
								</div>
								@endif
							</div>
						</div>
					</div>
				@endforeach
				</div>
				<div class="card-footer">
					<button class="btn btn-success">
						Submit
					</button>
					@if(count($sections)>1)
							@foreach($sections as $sec)
								<a href="{{ route('get_questions',['section'=>$sec->section]) }}"><button class="btn btn-primary">Section {{$sec->section}}</button></a>
							@endforeach
						@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection				
@section('footer-scripts')
<script>
	function addAnswer(a)
	{
		$.ajax({
			type: 'get',
			url: '/addAnswer',
			data: {
				'optid': a
			},
			success: function(res){document.getElementById(res.qusid).innerHTML = "<div class='card-footer answered'><button class='btn btn-sm' value=" + res.qusid + " onclick='removeAnswer(this.value)'>Remove Answer</button></div>";},
		});
	}
	function removeAnswer(a)
	{
		$.ajax({
			type: 'get',
			url: '/removeAnswer',
			data: {
				'qusid': a
			},
			success: function(res){document.getElementById(res.qusid).innerHTML = "<div class='card-footer unanswered'><h6>Not Answered</h6></div>";
				 document.getElementById(res.optid).checked = false;
				},
		});
	}
	function submitQuiz()
	{
		window.opener.location.href = "/submit-quiz-home";
		window.close()
		//location.replace("/submit-quiz-home")
		//window.close()
		// swal({
  //               title: "Are you sure?",
  //               text: "Once Submitted, you will not be able to resume it!",
  //               icon: "warning",
  //               buttons: true,
  //               dangerMode: true,
  //           })
  //               .then(() => {
  //                   if (willDelete) {
  //                       location.replace("https://www.w3schools.com")
  //                   } else {
  //                       swal("Cancelled Successfully");
  //                   }
  //               });
  
	}
</script>
@endsection()