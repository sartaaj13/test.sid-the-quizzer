@extends('Quiz.layouts.app')
@section('custom-styles')
@endsection()
@section('content')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">Quiz Rules</div>
					<div class="card-body">
						
						@if(count($section)==1)
							<button>Start Quiz</button>
						@endif
						@if(count($section)>1)
							@foreach($section as $sec)
								<a href="{{ route('get_questions',['section'=>$sec->section]) }}"><button>Section {{$sec->section}}</button></a>
							@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection()