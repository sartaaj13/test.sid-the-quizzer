@extends('frontend.layouts.app')
@section('custom-styles')
<style>
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection()
@section('content')
@include('sweet::alert')
<?php
	$isr = isset($r);
?>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header text-center">Read Rules Carefully</div>
				<div class="card-body">
					@if($isr==true)
					@foreach($r as $rule_heading => $rules)
					<div>
						<h3>{{ $rule_heading }}</h3>
					</div>
					<div>
						<ul>
							@foreach($rules as $r_rules)
								<li>
									<h4>{{ $r_rules }}</h4>
								</li>
							@endforeach
						</ul>
					</div>
					@endforeach
					@else
					<div>
						<h3 class="text-center">No Rules for this Quiz!!</h3>
					</div>
					@endif
				</div>
				<div class="card-footer">
					<div class="row justify-content-center">
						<button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Enter Details</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h3>Enter Your Details</h3>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				{{Form::open(['route' => 'add_user','method' => 'post','id'=>'add_user'])}}
				{{csrf_field()}}
				<label>Name</label>
				<input type="text" name="name" id="name" class="form-control" required="">
				<label>Email</label>  
				<input type="email" name="email" id="email" class="form-control" required="">
				<label>Id</label>
				<input type="number" name="tid" id="tid" class="form-control" required=""><br>
				<input type="submit" name="submit" class="btn btn-primary" value="Submit">
				{{Form::close()}}
			</div>
			<div class="modal-footer">
				<span style="font-family: myFont; font-size: 25px;" class="text-center">Crafted By: Inderjeet Singh</span>
			</div>
		</div>
	</div>
</div>
@endsection()