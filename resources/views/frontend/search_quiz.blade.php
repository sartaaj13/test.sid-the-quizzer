@extends('frontend.layouts.app')
@section('custom-styles')
<style>
	@font-face {
font-family: 'myFont';
src: url('{{asset('css/fonts/Billabong.ttf')}}');
}
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
@section('content')
@include('sweet::alert')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header text-center">Get Your Quiz</div>
					<div class="card-body">
						<div class="row justify-content-center">
							<div class="col-sm-6">
								<div class="card">
									{{Form::open(['route' => 'get_quiz','method' => 'post', 'id' => 'get_quiz'])}}
									{{ csrf_field() }}
									<div class="card-body">
										<label for="key">Enter Quiz Key</label>
										<input type="text" name="key" id="key" class="form-control" required="">
									</div>
									<div class="card-footer">
										<div class="row justify-content-center">
											<input type="submit" name="submit" class="btn btn-primary" value="Submit">
										</div>
									</div>
									{{Form::close()}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection()