@extends('backend.layouts.app')
@section('custom-styles')
<style>
    .pull-left{
        float: left;
    }
</style>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="container">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    <div class="col-sm-3 pull-left">
                        <div class="card">
                            <div class="card-header">Total Quizes</div>
                            <div class="card-body">
                                <h3 class="text-center">{{ count($quiz) }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 pull-left">
                        <div class="card">
                            <div class="card-header">Total Questions</div>
                            <div class="card-body">
                                <h3 class="text-center">{{ count($quiz) }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
