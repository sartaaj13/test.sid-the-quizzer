@extends('backend.layouts.app')
@section('custom-styles')
<style>
	.m-b-15{
		margin-bottom: 15px;
	}
	.pull-right{
		float: right;
	}
</style>
@section('content')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						Your Questions
						<a href="{{ route('delete_all') }}"><button class="pull-right btn btn-danger">Delete All</button></a>
					</div>
					<div class="card-body">
						@foreach($questions as $qus)
							<div class="row justify-content-center">
								<div class="col-sm-12 m-b-15">
									<div class="card">
										<div class="card-header">
											Section- {{ $qus->section }}
										<a href="{{ route('deletequestion',['id' => $qus->id]) }}"><button class="pull-right btn btn-danger">Delete</button></a>
										</div>
										<div class="card-body">
											<h3>Qus. {{ $qus->question }}</h3>
											@if($qus->codesnip!=null)
												<div class="card m-b-15">
													<div class="card-header">Code for the question</div>
													<div class="card-body">
														@foreach($qus->code as $code)
															<kbd>{{ $code }}</kbd><br>
														@endforeach
													</div>
												</div>
											@endif
											<div class="card">
												<div class="card-body">
													<?php $i=1;?>
													@foreach($qus->option as $options)
														@if($options->is_correct ==  1)
															<h3><?=$i?>. {{ $options->option }} <button class="btn btn-success">Correct</button>
															</h3>
														@else
															<h3><?=$i?>. {{ $options->option }} </h3>
														@endif
														<?php $i++?>
													@endforeach
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection()