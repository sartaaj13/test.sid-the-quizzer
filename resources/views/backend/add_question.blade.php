@extends('backend.layouts.app')
@section('custom-styles')
<style>
	.m-b-15{
		margin-bottom: 15px;
	}
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection()
@section('content')
@include('sweet::alert')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header text-center">Add Question</div>
				<div class="card-body">
					<div class="row justify-content-center m-b-15">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-header text-center">Format of Excel Sheet</div>
								<div class="card-body">
									<img src="{{ asset('images/excel_format.jpg') }}" style="width: 100%; border: 1px solid">
								</div>
								<div class="card-footer">
									<div class="row justify-content-center">
										<button class="btn btn-success">Download Sample Excel</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row justify-content-center m-b-15">
						<div class="col-sm-6">
							<div class="card">
								<div class="card-header">Add Questions Manually</div>
								<div class="card-body">
								{{Form::open(['route' => 'add_options', 'method' => 'get', 'id' => 'add_options'])}}
								{{ csrf_field() }}
								<label>Select from Active Quiz</label>
								<select id="quiz" name="quiz" class="form-control">
									@foreach($quiz as $quizes)
										<option value="{{ $quizes->id }}" >{{ $quizes->name }}</option>
									@endforeach
								</select>
								<label>Section(optional)</label>
								<input type="text" name="section" id="section" class="form-control">
								<label>Question Text</label>
								<input type="text" name="qus" id="qus" class="form-control" required="">
								<label>Code Snippet(Optional)</label>
								<textarea name="code" id="code" class="form-control"></textarea>
								<label>Option 1</label>
								<input type="text" name="opt1text" id="opt1text" class="form-control" required="">
								<label>Option 2</label>
								<input type="text" name="opt2text" id="opt2text" class="form-control" required="">
								<label>Option 3</label>
								<input type="text" name="opt3text" id="opt3text" class="form-control" required="">
								<label>Option 4</label>
								<input type="text" name="opt4text" id="opt4text" class="form-control" required="">
								<label>Choose Correct Option</label>
								<select name="correct" class="form-control">
									<option value="1">Option 1</option>
									<option value="2">Option 2</option>
									<option value="3">Option 3</option>
									<option value="4">Option 4</option>
								</select><br>
								<input type="submit" name="submit" id="submit" class="btn btn-primary" value="Submit Question">
								</div>
								{{Form::close()}}
							</div>
						</div>
						<div class="col-sm-6">
							<div class="card">
								<div class="card-header">Add Questions by Excel</div>
								<div class="card-body">
								{{Form::open(['route' => 'upload_excel', 'method' => 'post','enctype' => 'multipart/form-data' , 'id' => 'upload_excel'])}}
								{{ csrf_field() }}
									<label>Select from Active Quiz</label>
									<select id="quiz" name="quiz" class="form-control" onchange="getSection(this.value)">
										@foreach($quiz as $quizes)
											<option value="{{ $quizes->id }}" >{{ $quizes->name }}</option>
										@endforeach
									</select><br>
									<input type="file" name="excelfile" class="form-control-file"><br>
									<input type="submit" name="submit" value="Submit File" class="btn btn-primary">
								{{Form::close()}}
								</div>
								<div class="card-footer"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection()
@section('footer-scripts')
<script>
	function getSection(a)
	{
		$.ajax({
			type: 'get',
			url: '/get-sections',
			data: {
				'quizid': a
			},
			success: function(data){document.getElementById("#available").innerHTML = "<span>Available Sections</span>"},
		});
	}
</script>
@endsection()