@extends('backend.layouts.app')

@section('custom-styles')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection

@section('content')
@include('sweet::alert')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header text-center">Add New Quiz</div>
				<div class="card-body">
					<div class="row justify-content-center">
					<div class="col-sm-6">
						<div class="card">
							<div class="card-body">
								{{Form::open(['route' => 'add_quiz','method' => 'post', 'id' => 'add_quiz'])}}
								{{ csrf_field() }}
									{{Form::label('', 'Quiz Name', array('class' => 'form-control-label'))}}
									{{Form::text('quiz','', array('class' => 'form-control', 'placeholder' => 'Enter Quiz Name','required' => 'required'))}}
									{{Form::label('', 'Quiz Login Key', array('class' => 'form-control-label'))}}
									{{Form::text('loginkey','', array('class' => 'form-control', 'placeholder' => 'Enter Quiz Login Key','required' => 'required'))}}<br>
								<h6 class="text-center tag tag-default">Quiz Key will be auto generated</h6>
								<div class="row justify-content-center">
									<input type="submit" name="submit" value="Submit" class="btn btn-primary">
								</div>
								{{ Form::close() }}
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection