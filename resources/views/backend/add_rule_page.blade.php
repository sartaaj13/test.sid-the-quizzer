@extends('backend.layouts.app')
@section('custom-styles')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection()
@section('content')
@include('sweet::alert')
@if(Session::has('success-quiz'))
<script>
	window.open('/admin');
</script>
@endif
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header text-center">Add Rules</div>
				<div class="card-body">
					<div class="row justify-content-center">
						<div class="col-sm-8">
							<div class="card">
								{{Form::open([ 'route' => 'add_rule', 'method' => 'post','id' => 'add_rules'])}}
								{{ csrf_field() }}
								<div class="card-body">
									<label>Choose Quiz</label>
									<select name="quiz" id="quiz" class="form-control">
										@foreach($quiz as $quizes)
											<option value="{{ $quizes->id }}">{{ $quizes->name }}</option>
										@endforeach
									</select>
									<label>Add Heading</label>
									<input type="text" name="heading" id="heading" class="form-control">
									<label>Add Rules</label>
									<textarea name="rules" id="rules" class="form-control" placeholder="Rule line *Rule line 2* Rule Line3"></textarea>
								</div>
								<div class="card-footer">
									<div class="row justify-content-center">
										<input type="submit" name="submit" id="submit" value="Add Rules" class="btn btn-primary">
									</div>
								</div>
								{{Form::close()}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection()