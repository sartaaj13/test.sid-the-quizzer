@extends('backend.layouts.app')
@section('custom-styles')
<style>
	.margin-bottom{
		margin-bottom: 15px;
	}
	.margin-all-5px{
		margin: 5px;
	}
	.pull-left{
		float: left;
	}
</style>
@endsection()
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header text-center">Your Quizes</div>
				<div class="card-body">
					@foreach($quiz as $quizes)
							<div class="col-md-6 margin-bottom pull-left">
								<div class="card">
									<div class="card-header">Quiz Name - {{ $quizes->name }}</div>
									<div class="card-body">
										<h2>Quiz Key- '{{ $quizes->key }}'</h2>
										<h3>Login Key- '{{ $quizes->loginkey }}'</h3>
										<h4>Status- @if($quizes->active==1) Active @else Inactive @endif </h4>
									</div>
									<div class="card-footer">
										<div class="row justify-content-center">
											@if($quizes->active==1)<a href="{{ route('make-inactive',['id' => $quizes->id]) }}"><button class="btn btn-success margin-all-5px">Inactive</button></a> @else <a href="{{ route('make-active',['id' => $quizes->id]) }}"><button class="btn btn-success margin-all-5px">Active</button></a> @endif 
											
											<a href="{{ route('delete-quiz',['id' => $quizes->id]) }}"><button class="btn btn-danger margin-all-5px">Delete</button></a>
											<button class="btn btn-primary margin-all-5px">Edit</button>
										</div>
									</div>
								</div>
							</div>
					@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection()