@extends('master.layouts.app')
@section('custom-styles')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection()
@section('content')
@include('sweet::alert')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						Add Rules
					</div>
					<div class="card-body">
						<div class="row justify-content-center">
							<div class="col-sm-6">
								<div class="card">
									<div class="card-body">
										{{Form::open(['route' => 'q_m_addrules','method'=>'post','id'=>'q_m_addrules'])}}
										{{ csrf_field() }}
											<label>Add Heading</label>
											<input type="text" name="heading" id="heading" class="form-control">
											<label>Add Rules</label>
											<textarea name="rules" id="rules" class="form-control" placeholder="Rule line *Rule line 2* Rule Line3"></textarea><br>
											<div class="row justify-content-center">
												<input type="submit" name="submit" value="Add Rules" class="btn btn-primary">
											</div>
										{{Form::close()}}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection()