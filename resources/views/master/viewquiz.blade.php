@extends('master.layouts.app')
@section('custom-styles')
<style>
    .pull-left{
        float: left;
    }
</style>
@endsection()
@section('content')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<h3 class="text-center">{{ $response[0]->name }}</h3>
					</div>
					<div class="card-body">
						<div class="col-sm-3 pull-left">
	                        <div class="card">
	                            <div class="card-header">Total Questions in Quiz</div>
	                            <div class="card-body">
	                                <h3 class="text-center">{{ $quscout }}</h3>
	                            </div>
	                        </div>
                    	</div>
                    	<div class="col-sm-3 pull-left">
	                        <div class="card">
	                            <div class="card-header">Current Status</div>
	                            <div class="card-body">
	                            	@if($response[0]->active==1)
	                                <h3 class="text-center">Active</h3>
	                                @else
	                                <h3 class="text-center">Inactive</h3>
	                                @endif
	                            </div>
	                        </div>
                    	</div>
					</div>
					<div class="card-footer">
						<div class="row justify-content-center">
							<div class="col-sm-1">
								@if($response[0]->active==1)
									<a href="{{ route('q_m_inactives') }}"><button class="btn btn-secondary">Inactive</button></a>
								@else
									<a href="{{ route('q_m_actives') }}"><button class="btn btn-success">Active</button></a>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection()