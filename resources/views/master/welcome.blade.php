<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            @font-face {
            font-family: 'myFont';
            src: url('{{asset('css/fonts/Billabong.ttf')}}');
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="top-right links">
                   <span style="font-family: myFont; font-size: 20px;">Crafted By: Inderjeet Singh</span>
                </div>
            <div class="content">
                <div class="title m-b-md">
                    QuizMaster.SID
                </div>

                <div class="links">
                    @if(session()->has('quizmaster'))
                        <a href="{{ route('master_home') }}">Home</a>
                        <a href="{{ route('q_m_logout') }}">Logout</a>
                    @else
                        <a href="{{ route('q_m_login') }}">Login</a>
                    @endif
                </div>
            </div>
        </div>
    </body>
</html>
