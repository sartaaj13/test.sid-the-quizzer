@extends('master.layouts.app')
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">Participants</div>
				<div class="card-body">
					<table class="table">
						<thead>
							<th>#</th>
							<th>ID</th>
							<th>Participant Name</th>
							<th>Email Id</th>
							<th>Has Submitted</th>
						</thead>
						<?php $i=1?>
					@foreach($par as $p)
						<tbody>
							<tr>
								<td>{{$i}}</td>
								<td>{{ $p->tid }}</td>
								<td>{{ $p->name }}</td>
								<td>{{ $p->email }}</td>
								@if($p->issubmitted==1)
									<th>Submitted</th>
								@else
									<th>Not Submitted</th>
								@endif
						</tbody>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endsection()