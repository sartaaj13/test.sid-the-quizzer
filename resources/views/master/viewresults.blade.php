@extends('master.layouts.app')
@section('custom-styles')
@endsection()
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">{{ $quiz[0] }}</div>
				<div class="card-body">
					<table class="table">
						<thead>
							<th>#</th>
							<th>Participant Name</th>
							<th>Team ID</th>
							<th>Total Answered</th>
							<th>Correct Answered</th>
							<th>Wrong Answered</th>
							<th>Score</th>
						</thead>
						<?php $i=1?>
						@foreach($result as $res)
						<tbody>
							<tr>
								<td>{{$i}}</td>
								<td>{{ $res->name }}</td>
								<td>{{ $res->tid }}</td>
								<td>{{ $res->correct + $res->incorrect }}</td>
								<td>{{ $res->correct }}</td>
								<td>{{ $res->incorrect }}</td>
								<td>{{ $res->score }}</td>
							</tr>
						</tbody>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection()