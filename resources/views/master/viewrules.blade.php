@extends('master.layouts.app')
@section('custom-styles')
<style>
	.m-b-15
	{
		margin-bottom: 15px;
	}
	.pull-right{
		float: right;
	}
</style>
@endsection()
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">View Rules<a href="{{ route('q_m_delete_rule_all') }}"><button class="btn btn-danger pull-right">Delete All</button></a></div>
				<div class="card-body">
					@foreach($r as $rule_heading => $rules)
						<div class="row justify-content-center">
							<div class="col-sm-12 m-b-15">
								<div class="card">
									<div class="card-header">
										<h4>{{$rule_heading}}<a href="{{ route('q_m_delete_rule',['head' => $rule_heading]) }}"><button class="btn btn-danger pull-right">Delete</button></a> </h4>
									</div>
									<div class="card-body">
										<ul>
											@foreach($rules as $r_rules)
												<li>
													<h4>{{ $r_rules }}</h4>
												</li>
											@endforeach
										</ul>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endsection()