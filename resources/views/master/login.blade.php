<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        @font-face {
            font-family: 'myFont';
            src: url('{{asset('css/fonts/Billabong.ttf')}}');
            }
    </style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <span class="navbar-brand">
                    {{ config('app.name', 'Laravel') }}
                </span>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <span class="navbar-brand" style="font-family: myFont;">Crafted By: Inderjeet Singh</span>
                    </ul>
                </div>
            </div>
        </nav>
         <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                Quiz Master Login
                            </div>
                            <div class="card-body">
                                <div class="row justify-content-center">
                                    <div class="col-sm-6">
                                        <div class="card">
                                            <div class="card-body">
                                            {{Form::open(['route' => 'master_login','method' => 'post', 'id' => 'm-login'])}}
                                            {{ csrf_field() }}
                                            <label>Quiz Key</label>
                                            <input type="text" name="quizkey" id="quizkey" required="" class="form-control" placeholder="Enter Quiz Key">
                                            <label>Login Key</label>
                                            <input type="password" name="loginkey" id="loginkey" required="" class="form-control" placeholder="Login Key"><br>
                                            <div class="row justify-content-center">
                                                <input type="submit" name="submit" id="submit" value="Login" class="btn btn-primary">
                                            </div>
                                            {{Form::close()}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>
</html>
